package com.example.homework5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var model : UserModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getUsers()
    }

    private fun getUsers() {

        DataLoader.getRequest("users?page=2", object: CustomCallback{
            override fun onSuccess(result: String) {

                model = Gson().fromJson(result, UserModel::class.java)
                Log.d("myTag", "${model.data.size}")

                init(model)
            }
        })

    }

    private fun init(model: UserModel) {
        viewPager.adapter = ViewPagerAdapter(supportFragmentManager, model.data)
    }

    public fun getProfile(model : UserModel.Data) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.viewPager, ProfileFragment())
        transaction.commit()
    }
}
