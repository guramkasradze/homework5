package com.example.homework5

interface CustomCallback {
    fun onSuccess(result: String){}
    fun onFailure(errorMassage: String){}
}