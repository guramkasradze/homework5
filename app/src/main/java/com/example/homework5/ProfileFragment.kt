package com.example.homework5

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_profile.view.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    private lateinit var itemView: View
    lateinit var model: UserModel.Data

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        itemView = inflater.inflate(R.layout.fragment_profile, container, false)

        init()
        return itemView
    }

    private fun init() {
        Glide.with(this).load(model.avatar).into(itemView.profileImageView)
        itemView.profileIdTextView.text = "#" + model.id.toString()
        itemView.profileNameTextView.text = model.firstName + "  " + model.lastName
        itemView.profileMailTextView.text = model.email
    }

}
